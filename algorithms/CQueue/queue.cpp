#include <cassert>
#include <cstddef>
#include <iostream>
#include <ostream>

// The implementation of the CQueue class is based on a linked list, 
// which allows you to use the functiomality of the queue - specifically,
// a FIFO (first-in, first-out) data structure. 
// This class contains methods:
/*
 Push  - inserts a node with a value at the end.    Complexity: O( 1 )
 Pop   - removes the first node.                    Complexity: O( 1 )
 Front - access the first element.                  Complexity: O( 1 )
 Back  - access the last element.                   Complexity: O( 1 ) 
 Empty - checks whether the container is empty.     Complexity: O( 1 )
 Size  - returns the number of elements.            Complexity: O( 1 )
 Clear - destructs the queue.                       Complexity: O( n )
*/

//CQueue main class
template<typename T_>
class CQueue
{
    public:
            CQueue  ( void );
            ~CQueue ( void );
        
        void   Push  ( T_ x );
        void   Pop   ( void );
        int    Front ( void );
        int    Back  ( void );
        bool   Empty ( void );
        size_t Size  ( void );
        void   Clear ( void );
        template<typename U_>
        friend std::ostream & operator << ( std::ostream & os, const CQueue<U_> & x);
    private:
        class CNode
        {
            public:
                CNode  ( T_ x );
            CNode * m_Next;
            T_ m_Val;
        };

        CNode * m_Head;
        CNode * m_Tail;
        size_t m_Size;
};
//==============================================================
template<typename T_>
CQueue<T_>::CQueue  ( void )
            :   m_Head(nullptr),   
                m_Tail(nullptr),
                m_Size(0)
{
}
//--------------------------------------------------------------
template<typename T_>
CQueue<T_>::~CQueue ( void )
{
    Clear();
}
//--------------------------------------------------------------
template<typename T_>
void   CQueue<T_>::Push  ( T_ x )
{
    CNode *n = new CNode( x );
    if ( Empty() )
        m_Head = m_Tail = n;

    m_Tail->m_Next = n;
    m_Tail = n;
    m_Size++;
}
//--------------------------------------------------------------
template<typename T_>
void   CQueue<T_>::Pop   ( void )
{
    if ( Empty() )
        throw std::out_of_range("Queue is empty");
    
    CNode * tmp = m_Head;
    m_Head = m_Head->m_Next;
    delete tmp;
    m_Size--;
    if ( Empty() )
        m_Tail = nullptr;
}
//--------------------------------------------------------------
template<typename T_>
int CQueue<T_>::Front ( void )
{
    if ( Empty() ) 
        throw std::out_of_range("Queue is empty");
    return m_Head->m_Val;
}
//--------------------------------------------------------------
template<typename T_>
int CQueue<T_>::Back  ( void )
{
    if ( Empty() ) 
        throw std::out_of_range("Queue is empty");
    return m_Tail->m_Val;
}
//--------------------------------------------------------------
template<typename T_>
bool   CQueue<T_>::Empty ( void )
{
    return !m_Head;
}
//--------------------------------------------------------------
template<typename T_>
size_t CQueue<T_>::Size  ( void )
{
    return m_Size;
}
//--------------------------------------------------------------
template<typename T_>
void CQueue<T_>::Clear  ( void )
{
    while ( !Empty() )
    {
        Pop();
    }
}
//--------------------------------------------------------------
template<typename T_>
std::ostream & operator << ( std::ostream & os, const CQueue<T_> & x)
{
    auto * n = x.m_Head;
    while( n )
    {
        os << n->m_Val;
        if ( n->m_Next != nullptr )
            os << ", ";
        n = n->m_Next;
    }
    os << std::endl;
    return os;
}
//==============================================================
template<typename T_>
CQueue<T_>::CNode::CNode ( T_ x )
                :   m_Next(nullptr),
                    m_Val( std::move(x) )
{
}
//--------------------------------------------------------------
void test1()
{
    CQueue<int> a;
    a.Push(5);
    a.Push(10);
    a.Push(3);
    a.Push(6);
    std::cout << a; // 5, 10, 3, 6
    a.Pop();
    a.Pop();
    assert(a.Size() == 2);
    std::cout << a; // 3, 6
    a.Clear();
    assert(a.Size() == 0);
}
//--------------------------------------------------------------
void test2()
{
    CQueue<std::string> a;
    a.Push("This");
    a.Push("code");
    a.Push("is");
    a.Push("bulshit");
    std::cout << a; // This, code, is, bulshit
    a.Pop();
    a.Pop();
    assert(a.Size() == 2);
    std::cout << a; // is, bulshit
    a.Clear();
    assert(a.Size() == 0);
}


int main()
{
    test1();
    test2();
    return 0;
}