#include <cassert>
#include <iostream>
#include <ostream>
#include <utility>

// The implementation of the CSet class is based on a binary tree, 
// which allows you to quickly and efficiently insert, delete, and check for the presence of elements in a set. 
// This class contains methods:
/*
 Insert - inserts a node with a value into the desired location in the tree.   Complexity: O( log (n) )
 Delete - removes a node with a value from the tree.                           Complexity: O( log (n) )
 IsSet  - checks for the presence of a value by key.                           Complexity: O( log (n) )
 Size   - returns the number of elements in the set.                           Complexity: O( 1 ) 
*/

//CSet main class
template <typename T_>
class CSet 
{
    public:
            CSet              ( void );
            CSet              ( const CSet<T_> & src );
            CSet & operator = ( CSet<T_>   src );
            ~CSet             ( void );
        void   Swap   ( CSet<T_> & src );
        bool   Insert ( T_ x );
        bool   Delete ( const T_ & x );
        bool   IsSet  ( const T_ & x ) const;
        size_t Size   ( void  ) const;

        template<typename U_>
        friend std::ostream & operator << ( std::ostream & os, const CSet<U_> & src );
    private:
        class CNode
        {
            public:
                    CNode  ( T_ x );
                    CNode  ( CNode & src );
                    ~CNode ( void );
                CNode * Clone ( void );
                void    Print ( std::ostream & os, bool isFirst ) const;
                CNode * m_L;
                CNode * m_R;
                T_      m_Val;
        };
        size_t m_Size;
        CNode * m_Root;
};
//=============================================================================
template <typename T_>
CSet<T_>::CSet    ( void )
            :   m_Size(0),
                m_Root(nullptr)
{
}
//-----------------------------------------------------------------------------
template <typename T_>
CSet<T_>::CSet    ( const CSet<T_> & src )
            :   m_Size( src.m_Size ),
                m_Root( src.m_Root ? src.m_Root->Clone() : nullptr )
{
}
//-----------------------------------------------------------------------------
template <typename T_>
CSet<T_> & CSet<T_>::operator = ( CSet<T_> src )
{
    Swap(src);
    return *this;
}
//-----------------------------------------------------------------------------
template <typename T_>
CSet<T_>::~CSet ( void )
{
    delete m_Root;
}
//-----------------------------------------------------------------------------
template <typename T_>
void CSet<T_>::Swap ( CSet<T_> & src )
{
    std::swap( m_Root, src.m_Root );
    std::swap( m_Size, src.m_Size );
}
//-----------------------------------------------------------------------------
template <typename T_>
bool   CSet<T_>::Insert ( T_ x )
{
    CNode ** n = &m_Root;
    while (*n)
    {
        if ( x < (*n)->m_Val )
            n = &(*n)->m_L;
        else if ( (*n)->m_Val < x )
            n = &(*n)->m_R;
        else 
            return false;
    }
    *n = new CNode ( std::move(x) );
    m_Size++;
    return true;
}
//-----------------------------------------------------------------------------
template <typename T_>
bool   CSet<T_>::Delete ( const T_ & x )
{
    CNode ** n = &m_Root;
    while (*n)
    {
        if ( x < (*n)->m_Val )
            n = &(*n)->m_L;
        else if ( (*n)->m_Val < x )
            n = &(*n)->m_R;
        else 
        {
            CNode * toDel = *n;
            if ( toDel->m_L && toDel->m_R )
            {
                n = &(*n)->m_R;
                while ( (*n)->m_L )
                    n = &(*n)->m_L;
                toDel ->m_Val = (*n)->m_Val;
                toDel = *n;
            }
            if ( toDel->m_L )
                *n = toDel->m_L;
            else 
                *n = toDel->m_R;

            toDel->m_L = toDel->m_R = nullptr;
            delete toDel;
            m_Size--;
            return true;
        }
    }
    return false;
}
//-----------------------------------------------------------------------------
template <typename T_>
bool   CSet<T_>::IsSet  ( const T_ & x ) const
{
    CNode *n = m_Root;
    while ( n )
    {
        if ( x < n->m_Val )
            n = n->m_L;
        else if ( n->m_Val < x )
            n = n->m_R;
        else
            return true;
    }
    return false;
}
//-----------------------------------------------------------------------------
template <typename T_>
size_t CSet<T_>::Size ( void  ) const
{
    return m_Size;
}
//-----------------------------------------------------------------------------
template <typename T_>
std::ostream & operator << ( std::ostream & os, const CSet<T_> & src )
{
    os << "{";
    if ( src.m_Root )
        src.m_Root->Print(os, true);
    os << "}";
    return os;
}
//=============================================================================
template <typename T_>
CSet<T_>::CNode::CNode  ( T_ x )
                :   m_L   ( nullptr ),
                    m_R   ( nullptr ),
                    m_Val ( std::move( x ) )
{                   
}
//-----------------------------------------------------------------------------
template <typename T_>
CSet<T_>::CNode::CNode  ( CNode & src )
                :   m_L( src.m_L ? src.m_L->Clone() : nullptr ),
                    m_R( src.m_R ? src.m_R->Clone() : nullptr ),
                    m_Val( src.m_Val )
{
}
//-----------------------------------------------------------------------------
template <typename T_>
CSet<T_>::CNode::~CNode ( void )
{
    delete m_L;
    delete m_R;
}
//-----------------------------------------------------------------------------
template <typename T_>
typename CSet<T_>::CNode * CSet<T_>::CNode::Clone()
{
    return new CNode(*this);
}
//-----------------------------------------------------------------------------
template <typename T_>
void CSet<T_>::CNode::Print ( std::ostream & os, bool isFirst ) const
{
    if ( m_L )
    {
        m_L->Print(os, isFirst );
        isFirst = false;
    }
    if ( ! isFirst )
        std::cout << ", ";
    std::cout << m_Val;
    if ( m_R )
        m_R->Print(os, false);
}
//-----------------------------------------------------------------------------
void test1 ()
{
    CSet<int> a;
    assert(a.Insert( 100 ));
    assert(a.Insert( 50 ));
    assert(a.Insert( 150 ));
    assert(a.Insert( 10 ));
    assert(a.Insert( 60 ));
    assert(a.Insert( 15 ));
    assert(a.Insert( 52 ));
    assert(a.Insert( 200 ));
    assert(a.Insert( 120 ));
    assert(a.Insert( 305 ));

    assert(a.IsSet(15));
    assert(a.IsSet(120));
    assert(a.IsSet(10));
    assert(a.IsSet(52));
    assert(a.IsSet(305));

    assert( a.Size() == 10);
    std::cout << a << std::endl;

    assert(a.Delete( 15 ));
    assert(a.Delete( 200 ));
    assert(a.Delete( 52 ));
    assert(!a.Delete( 228 ));

    assert( a.Size() == 7 );

    std::cout << a << std::endl;
}

void test2 ()
{
    CSet<std::string> a;
    assert(a.Insert( "Tests" ));
    assert(a.Insert( "make" ));
    assert(a.Insert( "our" ));
    assert(a.Insert( "lives" ));
    assert(a.Insert( "easier" ));
    assert(a.Insert( "Don't" ));
    assert(a.Insert( "forget" ));
    assert(a.Insert( "about" ));
    assert(a.Insert( "it" ));
    assert(a.Insert( "NEVER" ));

    assert(a.IsSet("it"));
    assert(a.IsSet("our"));
    assert(a.IsSet("NEVER"));
    assert(a.IsSet("forget"));
    assert(a.IsSet("Tests"));

    assert( a.Size() == 10);
    std::cout << a << std::endl;

    assert(a.Delete( "NEVER" ));
    assert(a.Delete( "Don't" ));
    assert(a.Delete( "make" ));
    assert(!a.Delete( "IRON MAN" ));

    assert( a.Size() == 7 );

    std::cout << a << std::endl;
}


int main()
{
    test1();
    test2();
    return 0;
}